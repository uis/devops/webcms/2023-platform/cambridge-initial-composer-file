# Cambridge Initial Setup

This repo contains the initial `composer.json` file and local DDEV setup to create a local development environment.

## Install DDEV

Install instructions: https://ddev.readthedocs.io/en/stable/

## Drupal local site setup and management

### Initial setup

Start DDEV:

```bash
ddev start
```

Install new Drupal site:

```bash
ddev init
```

### Refresh and rebuild

To update Drupal profile, modules and theme and KEEP you current installation and Drupal content:

```bash
ddev refresh-drupal
```

To update Drupal profile, modules and theme and REINSTALL Drupal from scratch (will delete existing Drupal content):

```bash
ddev rebuild-drupal
```

## Storybook development

To start a watcher for Storybook Twig components and twig files:

```bash
cd web/themes/custom/cambridge_tailwind
ddev npm run watch
```

Then open https://cam.ddev.site:6006/ to see Storybook.

Note that the above command runs inside the ddev docker container.
All additional npm commands should also be run inside the container with

```bash
ddev npm [your command]
```
